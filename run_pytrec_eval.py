#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  pytrec_eval.py
#  
#  

import pytrec_eval
import json
import os
import glob
import re


def get_scoring_fnc_avgs(scoring_fnc, run_file_loc, qrel_file_loc, filename): 
    """ Retrieve and display the results (measure averages) for a given scoring function """
    try:
        with open(qrel_file_loc, 'r') as f_qrel:
            qrel = pytrec_eval.parse_qrel(f_qrel)
        with open(run_file_loc, 'r') as f_run:
            run = pytrec_eval.parse_run(f_run)
    except:
        print("Error opening run file", run_file_loc, "and/or qrel file ", qrel_file_loc)
        return 1
    evaluator = pytrec_eval.RelevanceEvaluator(
    qrel, {'Rprec', 'map', 'ndcg_cut'})
    rPrec_cnt = 0
    rPrec_total = 0
    map_cnt = 0
    map_total = 0
    ndcg_20_cnt = 0
    ndcg_20_total = 0
    for query_id, query_measures in sorted(evaluator.evaluate(run).items()):
        for measure, value in sorted(query_measures.items()):   
            if(measure == 'Rprec'):
                rPrec_cnt += 1
                rPrec_total += value	
            elif(measure == 'map'):
                map_cnt += 1
                map_total += value 
            elif(measure[:4] == 'ndcg' and measure[9:] == "20"):
                ndcg_20_cnt += 1
                ndcg_20_total += value
    display = scoring_fnc + "\n" 
    display += "\tRPrec: " + str(rPrec_total/rPrec_cnt) + "\n"
    display += "\tmap: " + str(map_total/map_cnt) + "\n"
    display += "\tndcg@20: " + str(ndcg_20_total/ndcg_20_cnt)
    #print(display)
    # output to a file for trec_eval comparisons 
    #filename = "trec_eval_results.txt" 
    if(os.path.exists(filename)):
        f = open(filename, 'a')
    else:
        f = open(filename,"w")
    f.write(display + "\n")
    f.close()
    return 0


def main(args):
    if(len(args) != 4):
        print("ERROR: script needs 3 args (qrel file path, run file path, IHo_sections)")
        return 1
    qrel_file_path = args[1]
    run_file_path = args[2]
    do_sections = args[3]
    if(do_sections == "True"):
        filename = "trec_eval_results_sections.txt"
    elif(do_sections == "False"): 
        filename = "trec_eval_results.txt"
    else:
        print("Invalid do_sections parameter (must be True or False)");
        sys.exit(1)
     
    if(os.path.exists(filename)):
        os.remove(filename)
    try:
        #for subdir, dirs, files in os.walk("./run_files"):
        for subdir, dirs, files in os.walk(run_file_path):
            for file in files:
                filepath = subdir + os.sep + file
                if filepath.endswith(".txt"):
                    pattern = "[^\/]+$"
                    prefix = re.search(pattern, filepath).group()[:3]                   
                    scoring_fnc = ""
                    if(prefix == "DEF"):
                        scoring_fnc = "default"                    
                    elif(prefix == "SUM"):
                        scoring_fnc = "sumHits"
                    elif(prefix == "LNC"):                     
                        scoring_fnc = "lnc.ltn"
                    elif(prefix == "BNN"):                     
                        scoring_fnc = "bnn.bnn"
                    elif(prefix == "ANC"):                    
                        scoring_fnc = "anc.apc"
                    if(get_scoring_fnc_avgs(scoring_fnc, filepath, qrel_file_path, filename) == 1):
                        return 1
    except:
        return 1
    return 0


if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
