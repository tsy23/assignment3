

/**
 * LuceneIndexer.java
 * 
 * UNH CS753
 * Progrmming Assignment 2
 * Group 2
 * 
 * This class is responsible for creating the index composed of documents
 * and terms. 
 * 
**/


package edu.unh.cs753.indexing;

import edu.unh.cs.treccar_v2.Data;
import edu.unh.cs753.utils.IndexUtils;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import java.util.ArrayList;
import java.io.IOException;
import java.io.FileNotFoundException;
import org.apache.lucene.index.IndexOptions;


public class LuceneIndexer {
    
    private final IndexWriter writer;

	/** 
	 * Construct a Lucene Indexer.
	 * @param indexLoc: the path containing the index.
    */
    public LuceneIndexer(String indexLoc) {
        writer = IndexUtils.createIndexWriter(indexLoc);
    }
    
    
    /**
     * Function: doIndex
     * Desc: Perform indexing on a cbor file (containing paragraphs).
     * @param cborLoc: The path containing the cbor file.
     */
    public void doIndex(String cborLoc) throws IOException, FileNotFoundException {
        int counter = 0;
        // Updated for term vectors (for term freq calculations)
        FieldType ft = new FieldType();
        ft.setIndexOptions( IndexOptions.DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS );
        ft.setStoreTermVectors( true );
        ft.setStoreTermVectorOffsets( true );
        ft.setStoreTermVectorPayloads( true );
        ft.setStoreTermVectorPositions( true );
        ft.setTokenized( true );
        for (Data.Paragraph p : IndexUtils.createParagraphIterator(cborLoc)) {
            Document doc = new Document();
            doc.add(new StringField("id", p.getParaId(), Field.Store.YES));
            doc.add(new Field("text", p.getTextOnly(), ft));
            writer.addDocument(doc);
            counter++;
            if (counter % 20 == 0) {
                writer.commit();
            }
        }
        writer.close();
    }

}
