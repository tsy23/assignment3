/**
 * LuceneSearcher.java
 * 
 * UNH CS753
 * Progrmming Assignment 3
 * Group 2
 * 
 * This class is responsible for the bulk of the work in this program.
 * After index creation, it is called upon repeatedly to perform searches
 * and evaluation routines per program 3 specifications. 
 * 
**/

package edu.unh.cs753.indexing;

import edu.unh.cs753.utils.SearchUtils;
import edu.unh.cs.treccar_v2.Data;
import org.apache.lucene.util.BytesRef;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.index.PostingsEnum;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.search.similarities.BasicStats;
import org.apache.lucene.search.similarities.SimilarityBase;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.document.Document;
import org.apache.lucene.store.FSDirectory;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.Random;
import java.io.PrintWriter;
import java.io.File;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Path;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.Color;
import java.text.DecimalFormat;
import javax.imageio.ImageIO;




public class LuceneSearcher { 
	
    public final IndexSearcher searcher;
    private static final int MAX_REL_DOCS = Integer.MAX_VALUE;
    private ArrayList<HashMap<String, Integer>> termFreqMaps; 
    private ArrayList<HashMap<String, Long>> docFreqMaps;
    private ArrayList<Integer> maxDocTermFreqs;
    private ArrayList<String> docIds;
    private SCORING_FNCS scoringFnc;
    private String paraFilePath;
    private String pageFilePath;
    private String indexLoc;
    private int nQueries;
    private int N;
    
    private final static int WIDTH = 1000, HEIGHT = 750;
    
    public enum SCORING_FNCS {
        DEF, SUM, LNC_LTN, BNN_BNN, ANC_APC
    }
    
    
    /** 
	 * Construct a Lucene Searcher.
	 * @param indexLoc: the path containing the index.
    */
    public LuceneSearcher(String indexLoc, String paraFilePath, 
                          String pageFilePath) throws IOException, FileNotFoundException {
                                       
        searcher = SearchUtils.createIndexSearcher(indexLoc);
        this.scoringFnc = SCORING_FNCS.DEF;
        searcher.setSimilarity(new BM25Similarity());
        termFreqMaps = new ArrayList<HashMap<String, Integer>>();
        docFreqMaps = new ArrayList<HashMap<String, Long>>();
        maxDocTermFreqs = new ArrayList<Integer>();
        docIds = new ArrayList<String>();
        this.paraFilePath = paraFilePath;
        this.pageFilePath = pageFilePath;
        this.indexLoc = indexLoc;
        fillMaps();  
    }
    
    
    public void setScoringFnc(SCORING_FNCS scoringFnc) throws IOException {
        this.scoringFnc = scoringFnc;
        if(this.scoringFnc == SCORING_FNCS.SUM) 
            this.sumHits();
    }
    
    /**
     * Function: query
     * Desc: Queries Lucene paragraph corpus using a standard similarity function.
     *       Note that this uses the StandardAnalyzer.
     * @param queryString: The query string that will be turned into a boolean query.
     * @param nResults: How many search results should be returned
     * @return TopDocs (ranked results matching query)
     */
    public TopDocs query(String queryString, Integer nResults) {
        Query q = SearchUtils.createStandardBooleanQuery(queryString, "text");
        try {
            return searcher.search(q, nResults);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    

	/**
     * Function: sumHits
     * Desc: A custom scoring function which is the sum of hits within a document.
     */
    public void sumHits() throws IOException {
        SimilarityBase mysimilarity= new SimilarityBase() {
            @Override
            protected float score(BasicStats basicStats, float freq, float docLen) {
                return freq;
            }

            @Override
            public String toString() {
                return null;
            }
        };
        searcher.setSimilarity(mysimilarity);
    }
    
    
    //--map making routines---------------------------------------------
    private void fillMaps() throws IOException {
        
        IndexReader reader = DirectoryReader.open(FSDirectory.open(new File(indexLoc).toPath()));
        this.N = reader.numDocs();
        for(int i = 0; i < N; i++) {
            HashMap<String, Integer> termFreqMap = new HashMap<String, Integer>();
            HashMap<String, Long> docFreqMap = new HashMap<String, Long>();
            Document doc = reader.document(i);
            String docID = doc.get("id");
            docIds.add(docID);
            Terms termVector = reader.getTermVector(i, "text");
            int maxTermFreq = 0;
            if(termVector != null) { // if null, term appeared in no documents
                TermsEnum itr = termVector.iterator();
                BytesRef term = null;
                PostingsEnum postings = null;
                while((term = itr.next()) != null) { 
                    String termText = term.utf8ToString();
                    postings = itr.postings(postings, PostingsEnum.ALL);
                    postings.nextDoc();
                    long docFreq = reader.docFreq(new Term("text", termText));
                    docFreqMap.put(termText, docFreq);
                    int freq = postings.freq();
                    if(freq > maxTermFreq)
                        maxTermFreq = freq;
                    termFreqMap.put(termText, freq);
                }
            }
            maxDocTermFreqs.add(maxTermFreq);
            termFreqMaps.add(termFreqMap);
            docFreqMaps.add(docFreqMap);
        }
    }
    
    
    private HashMap<String, AtomicInteger> getQueryFreqMap(String query) {
        
        HashMap<String, AtomicInteger> ret = new HashMap<String, AtomicInteger>();
        List<String> queryTerms = getQueryTerms(query);
        for(String queryTerm : queryTerms) {
            if(ret.containsKey(queryTerm)) 
                ret.get(queryTerm).incrementAndGet();
            else 
                ret.put(queryTerm, new AtomicInteger(1));
        }
        return ret;
    }

    
    //--run file routines-----------------------------------------------
    // for containing results
    private class Pair implements Comparable<Pair> {
        
        String docId;
        float score;
        
        Pair(String docId, float score) {
            this.docId = docId;
            this.score = score;
        }
        
        public String toString() {
            String ret = "";
            ret += "Pair: " + this.docId + ", " + this.score;
            return ret;
        }
        
        public String getDocId() {
            return this.docId;
        }
        
        public float getScore() {
            return this.score;
        }
        
        @Override
        public int compareTo(Pair another) {
            if(this.score > another.score)
                return -1;
            if(this.score < another.score)
                return 1;
            return 0;
        }
    }
    
    private float getDocVectorLen(HashMap<String, Integer> map, String query, int maxTermFreq) {
        List<String> queryTerms = getQueryTerms(query);
        float vectorLen = 0.0f;
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            String term = entry.getKey();
            if(queryTerms.contains(term)) {
                float tfId = entry.getValue();
                float freq = 0;
                if(scoringFnc == SCORING_FNCS.LNC_LTN) 
                    freq = 1 + (float)Math.log10(tfId);
                else 
                    freq = 0.5f + ((0.5f * tfId) / maxTermFreq);
                vectorLen += freq * freq;
            }
        }
        if(vectorLen > 0)
            vectorLen = 1 / (float)Math.sqrt(vectorLen);
        return vectorLen;
    }
    
    
    private List<String> getQueryTerms(String query) {
        List<String> ret = new ArrayList<String>();
        Analyzer analyzer = new StandardAnalyzer();
        try {
            TokenStream stream  = analyzer.tokenStream(null, new StringReader(query));
            stream.reset();
            while (stream.incrementToken()) {
                ret.add(stream.getAttribute(CharTermAttribute.class).toString());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return ret;
    }
    
    
    public void makeRunFile(int topN) throws IOException {
        ArrayList<ArrayList<Pair>> queryResults = null; 
        if(scoringFnc == SCORING_FNCS.DEF || scoringFnc == SCORING_FNCS.SUM) 
            queryResults = defOrSumRankedResults(topN);
        else if(scoringFnc == SCORING_FNCS.LNC_LTN) 
            queryResults = lncLtnRankedResults(topN);
        else if(scoringFnc == SCORING_FNCS.BNN_BNN) 
            queryResults = bnnBnnRankedResults(topN);
        else 
            queryResults = ancApcRankedResults(topN);
        String methodName = this.scoringFnc.toString();
        String outFileName = "run_files/" + methodName + "-runfile.txt";
        PrintWriter writer = new PrintWriter(outFileName, "UTF-8");
        int i = 0;
        for (Data.Page p : SearchUtils.createPageIterator(pageFilePath)) {
            String queryId = p.getPageId();
            String keywordQuery = p.getPageName();
            ArrayList<Pair> rankedResults = queryResults.get(i);
            int rank = rankedResults.size();
            for(Pair result : rankedResults) {
                String score = Float.toString(result.getScore());
                String docId = result.getDocId();
                String out = queryId + " Q0 " + docId + " " + rank--
                             + " " + score + " group2-" + methodName;		 
                writer.println(out);
            }
            i++;
        }
        writer.close();
    }
    
    
    
    public void makeRunFile(int topN, boolean doSections) throws IOException {
        
        if(!doSections) {
            makeRunFile(topN);
            return;
        }
         
        /*
        for (Data.Page p : SearchUtils.createPageIterator(pageFilePath)) {
            
            ArrayList<Pair> rankedResults = new ArrayList<Pair>();
            String queryId = p.getPageId();
            String keywordQuery = p.getPageName();
            
            List<Data.Page.SectionPathParagraphs> spParas = p.flatSectionPathsParagraphs();
            for( Data.Page.SectionPathParagraphs spps : spParas) {
                System.out.println("Page has a SectionPathParagraphs: " + spps);
                System.exit(9);
            }
        }
        */
    }
    
    
    
    
    private ArrayList<ArrayList<Pair>> defOrSumRankedResults(int topN) throws IOException {
        ArrayList<ArrayList<Pair>> queryResults = new ArrayList<ArrayList<Pair>>();
        for (Data.Page p : SearchUtils.createPageIterator(pageFilePath)) {
            ArrayList<Pair> rankedResults = new ArrayList<Pair>();
            String queryId = p.getPageId();
            String keywordQuery = p.getPageName();  
            TopDocs topDocs = query(keywordQuery, topN);
            int rank = topDocs.scoreDocs.length;
            // iterate through topN top docs and output to run file  
            for (ScoreDoc sd : topDocs.scoreDocs) { 
                Document doc = searcher.doc(sd.doc);
                String docId = doc.get("id");
                float score = sd.score;
                Pair pair = new Pair(docId, score);
                rankedResults.add(pair);
            }
            queryResults.add(rankedResults);
        }
        return queryResults;
    }
    
    
    
    private ArrayList<ArrayList<Pair>> lncLtnRankedResults(int topN) throws IOException {
        ArrayList<ArrayList<Pair>> queryResults = new ArrayList<ArrayList<Pair>>();
        for (Data.Page p : SearchUtils.createPageIterator(pageFilePath)) {
            String queryId = p.getPageId();
            String keywordQuery = p.getPageName();  
            // for queries with repeated terms
            HashMap<String, AtomicInteger> queryFreqMap = getQueryFreqMap(keywordQuery);
            ArrayList<Pair> rankedResults = new ArrayList<Pair>();
            // for each document
            for(int docIndex = 0; docIndex < N; docIndex++) {
                float docQueryProduct = 0.0f;
                HashMap<String, Integer> termFreqMap = termFreqMaps.get(docIndex);
                HashMap<String, Long> docFreqMap = docFreqMaps.get(docIndex);
                List<String> queryTerms = getQueryTerms(keywordQuery);
                for(String queryTerm : queryTerms) {
                    // if the document contains the query term, perform calculation
                    // else do nothing as it's value is 0
                    if(termFreqMap.containsKey(queryTerm)) {
                        //--do document calculations first-------
                        // (D1) calculate log term frequency
                        int tfTD = termFreqMap.get(queryTerm).intValue();
                        float docLogTermFreq = (float)(1 + Math.log10(tfTD));
                        // (D2) no document frequency
                        // (D3) calculate cosine normalization
                        float vectorLen = getDocVectorLen(termFreqMap, keywordQuery, -1);
                        float D = docLogTermFreq * vectorLen;
                        //--next do query calculations-----------
                        // (Q1) calculate log term frequency
                        tfTD = queryFreqMap.get(queryTerm).intValue(); 
                        float queryLogTermFreq = 1 + (float)Math.log10(tfTD);
                        // (Q2) calculate idf document frequency
                        long df = docFreqMap.get(queryTerm).intValue();
                        float idf = (float)Math.log10((float)N / df);
                        // (Q3) no normalization
                        float Q = queryLogTermFreq * idf;
                        docQueryProduct += Q * D;
                    } 
                }
                if(docQueryProduct > 0) { // if 0 don't to add to results
                    String docId = docIds.get(docIndex);
                    rankedResults.add(new Pair(docId, docQueryProduct));
                    if(rankedResults.size() == topN) // only want top 100
                        break;
                }
            } // sort (rank) the results
            Collections.sort(rankedResults);
            queryResults.add(rankedResults);
        }
        return queryResults;
    }
    

    private ArrayList<ArrayList<Pair>> bnnBnnRankedResults(int topN) throws IOException {
        ArrayList<ArrayList<Pair>> queryResults = new ArrayList<ArrayList<Pair>>();
        // iterate through each page
        for (Data.Page p : SearchUtils.createPageIterator(pageFilePath)) {
            String queryId = p.getPageId();
            String keywordQuery = p.getPageName();   
            ArrayList<Pair> rankedResults = new ArrayList<Pair>();
            // for each document
            for(int docIndex = 0; docIndex < N; docIndex++) {
                float docQueryProduct = 0.0f;
                HashMap<String, Integer> termFreqMap = termFreqMaps.get(docIndex);
                List<String> queryTerms = getQueryTerms(keywordQuery);
                for(String queryTerm : queryTerms) {
                    // if the document contains the query term, perform calculation
                    // else do nothing as it's value is 0
                    if(termFreqMap.containsKey(queryTerm)) {
                        //--do document calculations first-------
                        // (D1) calculate boolean term frequency
                        int docBoolTermFreq = 1;
                        // (D2) no document frequency
                        // (D3) no normalization
                        float D = docBoolTermFreq;
                        //--next do query calculations
                        // (Q1) calculate boolean term frequency
                        int queryBoolTermFreq = 1;
                        // (Q2) no document frequency
                        // (Q3) no normalization
                        float Q = queryBoolTermFreq;
                        docQueryProduct += Q * D; // redundant, always 1 but follow SMART formula
                    } 
                }
                if(docQueryProduct > 0) { // if it was 0, no reason to add it
                    String docId = docIds.get(docIndex);
                    rankedResults.add(new Pair(docId, docQueryProduct));
                    if(rankedResults.size() == topN) // only want top 100
                        break;
                }
            } // sort (rank) the results 
            Collections.sort(rankedResults);
            queryResults.add(rankedResults);
        }
        return queryResults;
    }
    
    

    private ArrayList<ArrayList<Pair>> ancApcRankedResults(int topN) throws IOException {
        
        ArrayList<ArrayList<Pair>> queryResults = new ArrayList<ArrayList<Pair>>();
        // iterate through each page
        for (Data.Page p : SearchUtils.createPageIterator(pageFilePath)) {
            String queryId = p.getPageId();
            String keywordQuery = p.getPageName();   
            // for queries with repeated terms
            HashMap<String, AtomicInteger> queryFreqMap = getQueryFreqMap(keywordQuery);
            ArrayList<Pair> rankedResults = new ArrayList<Pair>();
            // for each document
            for(int docIndex = 0; docIndex < N; docIndex++) {
                float docQueryProduct = 0.0f;
                HashMap<String, Integer> termFreqMap = termFreqMaps.get(docIndex);
                HashMap<String, Long> docFreqMap = docFreqMaps.get(docIndex);
                // need query cosine normalizing factor first
                float queryVectorLen = 0.0f;  
                // get the frequency of the term that appears most in the document
                int maxTermFreq = maxDocTermFreqs.get(docIndex);
                List<String> queryTerms = getQueryTerms(keywordQuery);
                for(String queryTerm : queryTerms) {
                    // proceed only if the query term appears in the document 
                    if(termFreqMap.get(queryTerm) == null) 
                        continue;
                    // (Q1) calculate augmented term frequency
                    int tfTd = queryFreqMap.get(queryTerm).intValue();
                    float queryAugTermFreq = 0.5f + ((0.5f * tfTd) / maxTermFreq); 
                    // (Q2) calculate prob idf document frequency
                    long df = docFreqMap.get(queryTerm);
                    float logVal = (float)Math.log10( (N-df)/df);
                    float probIdf = Math.max(0.0f, logVal); 
                    queryVectorLen += queryAugTermFreq * probIdf;
                }
                if(queryVectorLen == 0) 
                    continue;
                float docVectorLen = getDocVectorLen(termFreqMap, keywordQuery, maxTermFreq);
                for(String queryTerm : queryTerms) {
                    // if the document contains the query term, perform calculation
                    if(termFreqMap.containsKey(queryTerm)) {
                        //--do document calculations first-------
                        // (D1) calculate augmented term frequency
                        float termFreq = termFreqMap.get(queryTerm);
                        float docAugTermFreq = 0.5f + ((0.5f * termFreq) / maxTermFreq);
                        // (D2) no document frequency
                        // (D3) calculate cosine normalization
                        float D = docAugTermFreq * docVectorLen;
                        //--do query calculations
                        // (Q1) calculate augmented term frequency
                        int queryTermFreq = queryFreqMap.get(queryTerm).intValue();  
                        float queryAugTermFreq = 0.5f + ((0.5f * queryTermFreq) / maxTermFreq);
                        // (Q2) calculate prob idf document frequency
                        long df = docFreqMap.get(queryTerm);
                        float logVal = (float)Math.log10((N - df) / df);
                        float probIdf = Math.max(0.0f, logVal);
                        float augProbIdf = queryAugTermFreq * probIdf;
                        // (Q3) calculate cosine normalization
                        float Q = augProbIdf * queryVectorLen;
                        docQueryProduct += Q * D;
                    } 
                }
                if(docQueryProduct > 0) { // if it was 0, no reason to add it
                    String docId = docIds.get(docIndex);
                    rankedResults.add(new Pair(docId, docQueryProduct));
                    if(rankedResults.size() == topN) 
                        break;
                }
            } // sort (rank) the results
            Collections.sort(rankedResults);
            queryResults.add(rankedResults);
        }
        return queryResults;
    }
    
    
    public void doStdErrAnalysis() throws IOException {
        
        final int topN = 100;
        ArrayList<ArrayList<Pair>> results;
        if(scoringFnc == SCORING_FNCS.DEF || scoringFnc == SCORING_FNCS.SUM) 
            results = defOrSumRankedResults(topN); 
        else if(scoringFnc == SCORING_FNCS.LNC_LTN) 
            results = lncLtnRankedResults(topN);
        else if(scoringFnc == SCORING_FNCS.BNN_BNN) 
            results = bnnBnnRankedResults(topN);
        else // ANC_APC 
            results = ancApcRankedResults(topN);
        float stdErrTotal = 0;
        int i = 0;
        ArrayList<Float> queryStdErrVals = new ArrayList<Float>();
        for (Data.Page p : SearchUtils.createPageIterator(pageFilePath)) {
            String queryId = p.getPageId();
            String keywordQuery = p.getPageName();
            ArrayList<Pair> rankedResults = results.get(i);
            float mean = 0;
            for(Pair result : rankedResults) 
                mean += result.getScore();
            mean /= rankedResults.size();
            float sumSqDev = 0;
            for(Pair result : rankedResults) {
                float tmpScore = result.getScore();
                float deviation = mean - tmpScore;
                deviation *= deviation;
                sumSqDev += deviation;
            }
            int n = rankedResults.size();
            if(n > 1) {
                sumSqDev /= n - 1;
                float stdDev = (float) Math.sqrt(sumSqDev);
                float stdErr = stdDev / (float) Math.sqrt(n);
                stdErrTotal += stdErr;
                queryStdErrVals.add(stdErr);
            }
            i++;
        }
        
        plotPoints(queryStdErrVals, "StandardError");
        stdErrTotal /= i;
        System.out.println("Standard Error average for scoring function " +
                            scoringFnc.toString() + ": " + stdErrTotal);
    }
    
    
    public void doSpearmanCorrelation() throws IOException {
        

        SCORING_FNCS curScoringFnc = this.scoringFnc;
        
        // get the default ranked values
        setScoringFnc(SCORING_FNCS.DEF);
        ArrayList<ArrayList<Pair>> defResults = defOrSumRankedResults(MAX_REL_DOCS);
        // get the ranked values to compare to
        setScoringFnc(curScoringFnc);
        ArrayList<ArrayList<Pair>> otherResults = null; 
        if(scoringFnc == SCORING_FNCS.LNC_LTN) 
            otherResults = lncLtnRankedResults(MAX_REL_DOCS);
        else if(scoringFnc == SCORING_FNCS.BNN_BNN) 
            otherResults = bnnBnnRankedResults(MAX_REL_DOCS);
        else if(scoringFnc == SCORING_FNCS.ANC_APC)             
            otherResults = ancApcRankedResults(MAX_REL_DOCS);
        else {
            System.out.println("Invalid scoring function set for Spearman: " + scoringFnc.toString());
            System.exit(1);
        }
        ArrayList<Float> queryCorrelVals = new ArrayList<Float>();
        float spearmanTotal = 0;
        for(int i = 0; i < defResults.size(); i++) {
            ArrayList<Pair> X = defResults.get(i);
            ArrayList<Pair> Y = otherResults.get(i);
            int n = X.size();
            float sigmaX = 0;
            float sigmaY = 0;
            float sigmaXY = 0;
            float sigmaXSQ = 0;
            float sigmaYSQ = 0;
            for(int j = 0; j < n-1; j++) {
                sigmaX = sigmaX + X.get(j).getScore();
                sigmaY = sigmaY + Y.get(j).getScore();
                sigmaXY = sigmaXY + X.get(j).getScore() * Y.get(j).getScore();
                sigmaXSQ = sigmaXSQ + X.get(j).getScore() * X.get(j).getScore();
                sigmaYSQ = sigmaYSQ + Y.get(j).getScore() * Y.get(j).getScore();
            }
            float num = (n * sigmaXY - sigmaX * sigmaY);
            float denom = (float)Math.sqrt((n*sigmaXSQ - Math.pow(sigmaX, 2)) *
                                (n*sigmaYSQ - Math.pow(sigmaY, 2) ) );
            if(denom == 0) 
                queryCorrelVals.add(0.0f);
            else {                   
                float querySpearmanResult = num / denom;
                spearmanTotal += querySpearmanResult;
                queryCorrelVals.add(querySpearmanResult);
            }
        }
        spearmanTotal /= defResults.size();
        System.out.println(" Spearman Correlation for scoring function " +
                        this.scoringFnc.toString() + ": " + spearmanTotal);
        plotPoints(queryCorrelVals, "Spearman");
    }
    
    
    private void plotPoints(ArrayList<Float> queryCorrelVals, String which) throws IOException {

        String methodName = this.scoringFnc.toString();
        XYSeries series = new XYSeries("Data");
        int cnt = 0;
        float setMean = 0;
        for(Float val : queryCorrelVals) { 
            series.add(cnt++, val);
            setMean += val;
        }
        setMean /= cnt;
        DecimalFormat df = new DecimalFormat("#.#");
        String tmp = df.format(setMean);
        final XYSeriesCollection data = new XYSeriesCollection(series);
        final JFreeChart chart = ChartFactory.createScatterPlot(
            which + " Coeffiency: " + methodName,
            "Queries", 
            "values", 
            data, 
            PlotOrientation.VERTICAL,
            true,
            true,
            false);
        final ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(WIDTH, HEIGHT));
        double value = Double.parseDouble(tmp);
        ValueMarker marker = new ValueMarker(value);  
        marker.setPaint(Color.black);
        XYPlot plot = (XYPlot) chart.getPlot();
        plot.addRangeMarker(marker);
        BufferedImage bi = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_BGR);
        Graphics graphics = bi.getGraphics();
        chartPanel.setBounds(0, 0, WIDTH, HEIGHT);
        chartPanel.paint(graphics);
        ImageIO.write(bi, "png", new File("plots/" + which + "-" + methodName + ".png"));
    } 
}
