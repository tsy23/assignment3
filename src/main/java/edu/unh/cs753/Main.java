/**
 * Main.java
 * 
 * UNH CS753
 * Progrmming Assignment 3
 * Group 2
 * 
 * This is the Main file for our third programming assignment. It first
 * creates the run files for part 1, ===TODO FINISH THIS==== There is a script that runs
 * all of these steps and can be run with the command bash prog3.sh. 
 * 
**/



package edu.unh.cs753;

import edu.unh.cs753.indexing.LuceneSearcher;
import edu.unh.cs753.indexing.LuceneIndexer;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.BufferedReader;
import java.io.FileReader;



public class Main {
    
    private static String paraFilePath; 
    private static String pageFilePath;  
    private final static String indexPath = "paragraphs";
    private static LuceneSearcher searcher;
    
    /**
     * Function: main
     * Desc: facilitate the trec_eval and evaluation algorithms.
     * @param args: program args (only 1 to run which part)
     */
    public static void main(String[] args) throws IOException, FileNotFoundException, Exception {
        
        System.setProperty("file.encoding", "UTF-8");
        // must have arg4 be which part of the program to run
        
        String action; 
        try {
            paraFilePath = args[0];
            pageFilePath = args[1];
            action = args[3]; // qrel file is args[2] (not needed here)
        }
        catch(ArrayIndexOutOfBoundsException iob) {
            System.out.println("Error: not enough args provided");
            throw iob; 
        }
        
        if(action.equals("part1")) {
            // run the indexing mechanism (only once per project run)
            System.out.print("Creating index... ");
            LuceneIndexer indexer = new LuceneIndexer(indexPath);
            
            long startTime = System.currentTimeMillis();
            try {
                indexer.doIndex(paraFilePath);
            } catch(FileNotFoundException fnf) {
                throw fnf;
            }
            long elapsedTime = System.currentTimeMillis() - startTime;
            System.out.println(Math.round(((float)elapsedTime / 1000)
                           * 100.0) / 100.0 + " seconds");
                           
            try {
                searcher = new LuceneSearcher(indexPath, paraFilePath, pageFilePath);
            } catch(FileNotFoundException fnf) {
                System.out.println("Error creating lucene searcher");
                throw fnf;
            }
            
            // make the run files necessary for step 2
            makeRunFiles(); 
            
            // calculate standard error
            doStandardError(); 
        }
        else if(action.equals("displayResults")) 
            displayTrecEvalResults();
        else if(action.equals("spearmanCorrelation")) 
            doSpearman();
        else if(action.equals("sectionQueries")) 
            doSectionQueries();
        else {
            System.err.println("Invalid argument: " + action);
            throw new Exception();
        }
    }
    
    /**
     * Function: makeRunFiles
     * Desc: Create the run files.
     */
    private static void makeRunFiles() throws IOException {

        final int topN = 100;
        long startTime;
        long elapsedTime;

        // default
        System.out.print("Retrieving default results... ");
        startTime = System.currentTimeMillis();
        searcher.makeRunFile(topN);
        elapsedTime = System.currentTimeMillis() - startTime;
        System.out.println(Math.round(((float)elapsedTime / 1000)
                           * 100.0) / 100.0 + " seconds");

        // lcn.ltn
        System.out.print("Retrieving lnc.ltn results... ");
        startTime = System.currentTimeMillis();
        searcher.setScoringFnc(LuceneSearcher.SCORING_FNCS.LNC_LTN);
        searcher.makeRunFile(topN);
        elapsedTime = System.currentTimeMillis() - startTime;
        System.out.println(Math.round(((float)elapsedTime / 1000)
                           * 100.0) / 100.0 + " seconds");
        // bnn.bnn
        startTime = System.currentTimeMillis();
        System.out.print("Retrieving bnn.bnn results... ");
        searcher.setScoringFnc(LuceneSearcher.SCORING_FNCS.BNN_BNN);
        searcher.makeRunFile(topN);
        elapsedTime = System.currentTimeMillis() - startTime;
        System.out.println(Math.round(((float)elapsedTime / 1000)
                           * 100.0) / 100.0 + " seconds");
        // anc.apc
        startTime = System.currentTimeMillis();
        System.out.print("Retrieving anc.apc results... ");
        searcher.setScoringFnc(LuceneSearcher.SCORING_FNCS.ANC_APC);
        searcher.makeRunFile(topN);
        elapsedTime = System.currentTimeMillis() - startTime;
        System.out.println(Math.round(((float)elapsedTime / 1000)
                           * 100.0) / 100.0 + " seconds");
                           
    }
    
    
    private static void doStandardError() throws IOException {
        System.out.println("\nStandard Error results");
        
        searcher.setScoringFnc(LuceneSearcher.SCORING_FNCS.DEF);
        searcher.doStdErrAnalysis();
        
        searcher.setScoringFnc(LuceneSearcher.SCORING_FNCS.LNC_LTN);
        searcher.doStdErrAnalysis();
        
        searcher.setScoringFnc(LuceneSearcher.SCORING_FNCS.BNN_BNN);
        searcher.doStdErrAnalysis();
        
        searcher.setScoringFnc(LuceneSearcher.SCORING_FNCS.ANC_APC);
        searcher.doStdErrAnalysis();
        
    }
    
    
    /**
     * Function: doSpearman
     * Desc: Do Spearman's rank correlation coefficient.
     */
    private static void doSpearman() throws IOException {
        
        try {
            searcher = new LuceneSearcher(indexPath, paraFilePath, pageFilePath);
        } catch(FileNotFoundException fnf) {
            System.out.println("Error creating lucene searcher");
            throw fnf;
        }
        
        System.out.println("\nCalculating Spearman coefficiency for lnc.ltn...");
        searcher.setScoringFnc(LuceneSearcher.SCORING_FNCS.LNC_LTN);
        searcher.doSpearmanCorrelation();
        
        System.out.println("Calculating Spearman coefficiency for bnn.bnn...");
        searcher.setScoringFnc(LuceneSearcher.SCORING_FNCS.BNN_BNN);
        searcher.doSpearmanCorrelation();
        
        System.out.println("Calculating Spearman coefficiency for anc.apc...");
        searcher.setScoringFnc(LuceneSearcher.SCORING_FNCS.ANC_APC);
        searcher.doSpearmanCorrelation();
        
    }
    
    
    /**
     * Function: doSectionQueries
     * Desc: Repeat of step 1 with section queries.
     */
    private static void doSectionQueries() throws IOException {
        
        /*
        System.out.println("\nRepeating step 1 but with page sections...");

        final int topN = 100;
        
        try {
            searcher = new LuceneSearcher(indexPath, paraFilePath, pageFilePath);
        } catch(FileNotFoundException fnf) {
            System.out.println("Error creating lucene searcher");
            throw fnf;
        }
        
        searcher.makeRunFile(topN, true);
        */
        
    }
    
   
    /**
     * Function: displayTrecEvalResults
     * Desc: Display trec eval results to console.
     */
    private static void displayTrecEvalResults() throws IOException {
        System.out.println("\nTrec_eval results:");
        try (BufferedReader br = new BufferedReader(new FileReader("trec_eval_results.txt"))) {
            String line;
            while ((line = br.readLine()) != null) 
                System.out.println(line);
        }
    }
    
    
}
