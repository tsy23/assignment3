

#
# This script updates the assignment3 gitlab repository. 
# 


# add changes in local repo
git add .

# if commit message given as (1 and only 1) arg, use it
# otherwise open gedit through the terminal for message
if [ $# -eq 0 ]
  then
    echo "No arguments supplied, opening terminal text editor"
    git commit
elif [ "$#" -ne 1 ];
    then
    echo "Illegal number of parameters: $#"
    exit 1
else
    git commit  -m '$1'
fi

# push changes after commit
git push origin

git push origin HEAD:master --force
