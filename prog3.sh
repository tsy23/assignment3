#!/bin/bash
#
# CS873 Programming Assignment2 main script Team2
#
# This script runs the necessary steps for parts 1 and 2 of programming 
# assignment 2. 
# 
# 1) maven compile
# 2) run the maven jar file for part 1
# 3) run the python script that uses pytrec_eval to get results for part 2
# 4) run the maven jar file for part 3
# 5) run the maven jar file for part 5
# 6) run the maven jar file for part 6
#
#


start_time="$(date -u +%s.%N)"

if [ "$#" -ne 3 ] && [ "$#" -ne 4 ]; then
    echo "Illegal number of parameters (must be 3 or 4)"
    exit 1
fi

if [ "$#" -eq 4 ]; then
    echo "Using pytrec_eval script"
    if [ "$4" != "-pytrec" ] && [ "$4" != "-p" ]; then
        echo "Invalid fourth prameter (-pytrec or -p)"
        exit 1
    fi
else
    echo "Not using pytrec_eval script, using existing results file (trec_eval_results.txt)"
fi

# remove program created files and directories (if they exist)
rm -rf ./run_files
rm -rf ./paragraphs 
rm -rf ./plots
if [ "$#" -eq 4 ]; then
    rm -rf trec_eval_results.txt
fi
rm -rf ./section_run_files


# maven compile command
mvn clean compile assembly:single
if [ $? -eq 1 ]; then
    echo "Maven compile failed"
    exit 1
fi

# create directories
mkdir ./run_files
#mkdir ./section_run_files
mkdir ./plots


#System.out.println("\n\n===Programming Assignment 2 Team 2===");
printf "\n\n===Programming Assignment 3 Team 2===\n"


# run the maven jar file (for part 1)
java -jar target/team2_1-1.0-SNAPSHOT-jar-with-dependencies.jar $1 $2 $3 part1
if [ $? -eq 1 ]; then
    echo "Error running jar file (part1)"
    exit 1
fi

if [ "$#" -eq 4 ]; then
    echo "Running python pytrec_eval script (run_pytrec_eval.py)"
    # run the python script that uses pytrec_eval to get results (for part 2)
    python3 run_pytrec_eval.py $3 ./run_files False
    if [ $? -eq 1 ]; then
        echo "Error running pytrec_eval script (run_pytrec_eval)"
        exit 1
    fi
fi


# run the maven jar file (to display results)
java -jar target/team2_1-1.0-SNAPSHOT-jar-with-dependencies.jar $1 $2 $3 displayResults
if [ $? -eq 1 ]; then
    echo "Error running jar file (displayResults)"
    exit 1
fi

# run the maven jar file (to run Spearman correlation routine)
java -jar target/team2_1-1.0-SNAPSHOT-jar-with-dependencies.jar $1 $2 $3 spearmanCorrelation
if [ $? -eq 1 ]; then
    echo "Error running jar file (spearmanCorrelation)"
    exit 1
fi


# run the maven jar file (to run sectionQueries)
java -jar target/team2_1-1.0-SNAPSHOT-jar-with-dependencies.jar $1 $2 $3 sectionQueries
if [ $? -eq 1 ]; then
    echo "Error running jar file (sectionQueries)"
    exit 1
fi


#if [ "$#" -eq 4 ]; then
    #echo "Running python pytrec_eval script (run_pytrec_eval.py)"
    # run the python script that uses pytrec_eval to get results (for part 2)
    #python3 run_pytrec_eval.py $3 ./section_run_files
    #if [ $? -eq 1 ]; then
        #echo "Error running pytrec_eval script (run_pytrec_eval)"
        #exit 1
    #fi
#fi


end_time="$(date -u +%s.%N)"
elapsed="$(bc <<<"$end_time-$start_time")"
echo "Program run time: $elapsed seconds"

echo "====================================="
